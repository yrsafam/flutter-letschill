class IconsMap {
  static Map<String, String> icons = {
    "logo": "lib/assets/images/icons/files/logo-big.svg",
    "eye": "lib/assets/images/icons/files/eye.svg",
    "eye-lock": "lib/assets/images/icons/files/eye-lock.svg",
    "logo-bg-big": "lib/assets/images/icons/files/logo-bg-big.svg",
    "logo-appbar": "lib/assets/images/icons/files/logo-appbar.svg",
    "burger": "lib/assets/images/icons/files/burger.svg",
    "emoji-home": "lib/assets/images/icons/files/emoji-home.svg",
    "logo-bg-home": "lib/assets/images/icons/files/logo-bg-home.svg",
    "avatar": "lib/assets/images/avatar.jpg",
  };

  String getIcon(String key) {
    return icons[key];
  }
}

final iconsMap = IconsMap();
