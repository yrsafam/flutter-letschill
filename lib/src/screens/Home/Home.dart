import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stay_home_flutter/src/screens/Home/widgets/Header.dart';

class ScreenHome extends StatelessWidget {
  const ScreenHome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [Header()],
      ),
    );
  }
}
