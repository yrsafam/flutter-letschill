import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:stay_home_flutter/assets/images/icons/index.dart';
import 'package:stay_home_flutter/src/constants/colors.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
      decoration: const BoxDecoration(
        gradient: RadialGradient(
          colors: <Color>[Colors.white, Color.fromRGBO(241, 241, 255, 1)],
          stops: <double>[0.34, 1],
        ),
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(30),
          bottomRight: Radius.circular(30),
        ),
        boxShadow: [
          BoxShadow(
            blurRadius: 16,
            offset: Offset(0, 8),
            color: Color.fromRGBO(130, 95, 185, 0.24),
          ),
        ],
      ),
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SvgPicture.asset(iconsMap.getIcon('logo-appbar')),
                SvgPicture.asset(iconsMap.getIcon('burger')),
              ],
            ),
          ),
          Container(
            height: 1,
            decoration: const BoxDecoration(
              color: Color.fromRGBO(219, 227, 240, 1),
            ),
          ),
          Stack(
            children: [
              Container(
                alignment: Alignment.topCenter,
                padding: const EdgeInsets.only(top: 15),
                child: SvgPicture.asset(
                  iconsMap.getIcon('logo-bg-home'),
                  width: MediaQuery.of(context).size.width - 32,
                ),
              ),
              Positioned(
                child: Align(
                  alignment: Alignment.topCenter,
                  child: SvgPicture.asset(
                    iconsMap.getIcon('emoji-home'),
                  ),
                ),
              ),
              Positioned(
                top: 96,
                left: MediaQuery.of(context).size.width / 2 - 20,
                child: Align(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(40),
                    child: Container(
                      width: 40,
                      height: 40,
                      padding: const EdgeInsets.all(4),
                      decoration: const BoxDecoration(
                        color: Colors.white,
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(32),
                        child: Image.asset(
                          iconsMap.getIcon('avatar'),
                          width: 32,
                          height: 32,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 140,
                left: 0,
                right: 0,
                child: Column(
                  children: const [
                    Text(
                      'Екатерина Петровна',
                      style: TextStyle(
                        color: ColorsCustom.text,
                        fontSize: 14,
                        fontFamily: 'Proxima',
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 8),
                      child: Text(
                        'Лютый бездельник',
                        style: TextStyle(
                          color: Color.fromRGBO(205, 173, 255, 1),
                          fontSize: 14,
                          fontFamily: 'Proxima',
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Positioned(
                bottom: 16,
                left: 0,
                right: 0,
                child: Center(
                  child: Container(
                    width: MediaQuery.of(context).size.width - 32,
                    height: 24,
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: const Color.fromRGBO(0, 0, 0, 0.04),
                      ),
                      borderRadius: const BorderRadius.horizontal(
                        left: Radius.circular(32),
                        right: Radius.circular(32),
                      ),
                      color: Colors.white,
                    ),
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            height: 22,
                            width:
                                (MediaQuery.of(context).size.width * 65) / 100,
                            decoration: const BoxDecoration(
                              color: ColorsCustom.accent,
                              borderRadius: BorderRadius.horizontal(
                                left: Radius.circular(32),
                                right: Radius.circular(32),
                              ),
                            ),
                          ),
                        ),
                        Container(
                          height: 12,
                          width: (MediaQuery.of(context).size.width * 65) / 100,
                          decoration: const BoxDecoration(
                            gradient: LinearGradient(
                              transform: GradientRotation(90),
                              stops: [
                                0,
                                2,
                              ],
                              colors: [
                                Color.fromRGBO(255, 255, 255, 0.24),
                                Color.fromRGBO(255, 255, 255, 0),
                              ],
                            ),
                            borderRadius: BorderRadius.horizontal(
                              left: Radius.circular(32),
                              right: Radius.circular(32),
                            ),
                          ),
                        ),
                        Center(
                          child: RichText(
                            text: const TextSpan(
                              style: TextStyle(
                                  fontSize: 16, fontFamily: 'Proxima'),
                              children: [
                                TextSpan(text: '870  '),
                                TextSpan(
                                  text: '/ 1000',
                                  style: TextStyle(
                                    color: Color.fromRGBO(255, 255, 255, 0.24),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
