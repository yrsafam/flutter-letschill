import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:stay_home_flutter/assets/images/icons/index.dart';
import 'package:stay_home_flutter/src/constants/colors.dart';
import 'package:stay_home_flutter/src/widgets/Button/main.dart';
import 'package:stay_home_flutter/src/widgets/Input/main.dart';

class ScreenLogin extends StatelessWidget {
  const ScreenLogin({Key key, @required this.onLogin}) : super(key: key);

  final VoidCallback onLogin;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
          height: MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.top,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              children: [
                const Logo(),
                const Input(label: 'Телефон'),
                const Input(
                  label: 'Пароль',
                  isPassword: true,
                ),
                const ButtonRestorePassword(),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Button(
                        onPress: onLogin,
                        title: 'Войти',
                        alignment: Alignment.topCenter,
                      ),
                      const ButtonGoRegistration(),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class Logo extends StatelessWidget {
  const Logo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Positioned(
          child: Align(
            alignment: Alignment.topCenter,
            child: SvgPicture.asset(
              iconsMap.getIcon('logo-bg-big'),
              width: MediaQuery.of(context).size.width - 32,
            ),
          ),
        ),
        Positioned(
          child: Align(
            child: SvgPicture.asset(
              iconsMap.getIcon('logo'),
            ),
          ),
        )
      ],
    );
  }
}

class ButtonRestorePassword extends StatelessWidget {
  const ButtonRestorePassword({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 12),
      child: GestureDetector(
        child: const Align(
          alignment: Alignment.topRight,
          child: Text(
            'Забыли пароль?',
            style: TextStyle(
              fontFamily: 'Proxima',
              fontSize: 14,
              color: Color.fromRGBO(117, 127, 140, 0.64),
            ),
          ),
        ),
      ),
    );
  }
}

class ButtonGoRegistration extends StatelessWidget {
  const ButtonGoRegistration({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(0, 19, 0, 37),
      alignment: Alignment.topCenter,
      child: GestureDetector(
        child: const Text(
          'Регистрация',
          style: TextStyle(
            fontSize: 16,
            color: ColorsCustom.titleDark,
            fontWeight: FontWeight.bold,
            fontFamily: 'Proxima',
          ),
        ),
      ),
    );
  }
}
