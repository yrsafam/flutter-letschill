import 'package:flutter/cupertino.dart';

class ColorsCustom {
  static const Color labelText = Color.fromRGBO(117, 127, 140, 0.72);
  static const Color text = Color.fromRGBO(51, 51, 51, 1);
  static const Color error = Color.fromRGBO(239, 90, 4, 1);
  static const Color borderBottom = Color.fromRGBO(219, 227, 240, 1);
  static const Color shadowButtonPrimary = Color.fromRGBO(130, 95, 185, 0.16);
  static const Color titleDark = Color.fromRGBO(49, 67, 112, 1);
  static const Color accent = Color.fromRGBO(138, 79, 232, 1);
}
