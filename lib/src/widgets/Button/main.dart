import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:stay_home_flutter/src/constants/colors.dart';

class Button extends StatelessWidget {
  const Button({
    @required this.onPress,
    @required this.title,
    this.alignment = Alignment.topLeft,
    this.margin = EdgeInsets.zero,
  });

  final VoidCallback onPress;
  final String title;
  final Alignment alignment;
  final EdgeInsets margin;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment,
      margin: margin,
      child: GestureDetector(
        onTap: onPress,
        child: Container(
          decoration: BoxDecoration(
            color: ColorsCustom.accent,
            borderRadius: BorderRadius.circular(40),
            boxShadow: const [
              BoxShadow(
                color: ColorsCustom.shadowButtonPrimary,
                blurRadius: 16,
                offset: Offset(0, 8),
              )
            ],
          ),
          width: MediaQuery.of(context).size.width - 32,
          height: 48,
          alignment: Alignment.center,
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontFamily: 'Proxima',
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
