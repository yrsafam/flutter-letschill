import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:stay_home_flutter/assets/images/icons/index.dart';
import 'package:stay_home_flutter/src/constants/colors.dart';

final TextEditingController _controller = TextEditingController();

class Input extends StatefulWidget {
  const Input({
    @required this.label,
    this.width,
    this.error = '',
    this.isPassword = false,
  });

  final String label;
  final String error;
  final double width;
  final bool isPassword;

  @override
  State<Input> createState() => _Input();
}

class _Input extends State<Input> {
  bool showPassword = false;

  void toggleShowPassword() {
    setState(() {
      showPassword = !showPassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width,
      child: TextFormField(
        controller: _controller,
        obscureText: widget.isPassword && !showPassword,
        style: const TextStyle(
          color: ColorsCustom.text,
          fontSize: 14,
        ),
        decoration: InputDecoration(
          errorText: widget.error.isNotEmpty ? widget.error : null,
          suffix: widget.isPassword
              ? GestureDetector(
                  onTap: toggleShowPassword,
                  child: SvgPicture.asset(
                    iconsMap.getIcon(showPassword ? 'eye' : 'eye-lock'),
                  ),
                )
              : null,
          labelText: widget.label,
          labelStyle: const TextStyle(
            color: ColorsCustom.labelText,
            fontSize: 14,
            fontFamily: 'Proxima',
          ),
          hintStyle: const TextStyle(
            color: ColorsCustom.text,
            fontSize: 14,
            fontFamily: 'Proxima',
          ),
          errorStyle: const TextStyle(color: ColorsCustom.error),
          enabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide(color: ColorsCustom.borderBottom),
          ),
          errorBorder: const UnderlineInputBorder(
            borderSide: BorderSide(
              color: ColorsCustom.error,
            ),
          ),
          focusedErrorBorder: const UnderlineInputBorder(
            borderSide: BorderSide(
              color: ColorsCustom.error,
            ),
          ),
          focusedBorder: const UnderlineInputBorder(
            borderSide: BorderSide(
              color: ColorsCustom.borderBottom,
            ),
          ),
        ),
      ),
    );
  }
}
