import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:stay_home_flutter/src/screens/Home/Home.dart';
import 'package:stay_home_flutter/src/screens/Login.dart';

void main() {
  runApp(Root());
}

class Root extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Root();
}

class _Root extends State<Root> {
  bool isAuth = false;

  void onLogin() {
    setState(() {
      isAuth = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Navigator(
        pages: [
          MaterialPage(
            key: const ValueKey('ScreenLogin'),
            name: 'ScreenLogin',
            child: ScreenLogin(
              onLogin: onLogin,
            ),
          ),
          if (isAuth) const MaterialPage(child: ScreenHome())
        ],
        onPopPage: (route, result) => route.didPop(result),
      ),
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          systemOverlayStyle: SystemUiOverlayStyle(
            statusBarBrightness: Brightness.dark,
            statusBarColor: Colors.white,
          ),
        ),
      ),
    );
  }
}
